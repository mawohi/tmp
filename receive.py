#!/usr/bin/python 
from flask import Flask, request 
from base64 import b64decode 
app = Flask(__name__) 
@app.route('/', methods=['POST']) 
def store_data(): 
    with open("test.txt", "ab") as f: 
        chunk = b64decode(request.data) 
        f.write(chunk) 
    return '1' 
if __name__ == '__main__': 
    app.debug = True 
    app.run("0.0.0.0", 6000)